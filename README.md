# Overview
This is the codebase for the autonomous ball-finding robot.

video: https://www.youtube.com/watch?v=nFCm26sxP7o&t=70s

The robot logic code can be found in TeamCode/src/main/java/org/firstinspires/ftc/teamcode/BallFindingAuto.java

The vision and pathfinding code can be found in 
FTCRobotController/src/main/jni

![picture](images/RobotPicture.png)

# Method
This problem can be divided into three parts. First was wiffle ball localization. To do this, I used C++ and the OpenCV vision library to generate a hue-filter mask to isolate blue or red balls. Then, I performed blob detection to isolate groups of white pixels in the mask. Finally, each blob was assigned a score based on roundness, size, color intensity, and vertical distance from the horizon (to prefer closer balls). The ball with the highest score was selected. To navigate the robot to the ball without hitting obstacles along the way, I implemented a modified version of the A* pathfinding algorithm. This step went through many iterations due to all the mechanical constraints of the robot that the algorithm needed to account for. At first, the additional constraints prevented the algorithm from finding a valid path. This part also exercised my optimization skills, as my first Java prototypes were not fast enough to compute the path in a reasonable time. The last step involved shooting the ball into a goal. This utilized a free-wheel position tracking system to navigate to the goal and shoot the ball. Although challenging, I enjoy working on these hard problems, and I enjoy the process of iterating on many different solutions.

